#!/bin/sh

backup_path=/root/backup
current_date=`date +%Y-%m-%d-%H-%M-%S`

if [ -z "$DB_NAME" ] || [ -z "$BUCKET" ]; then
    echo "BUCKET and DB_NAME need to exist in the env"
    exit 1
fi

echo "##########################################################"
echo `date`
echo "backing up $DB_NAME to $current_date.zip"
echo "and uploading to S3 bucket: $BUCKET..."
echo "##########################################################"

# make the backup directory unless it exists
if [ -d $backup_path ]; then
    echo "$backup_path already exists, cleaning it out..."
    rm -rf $backup_path/$DB_NAME
else
    echo "creating $backup_path"
    mkdir $backup_path
fi
cd $backup_path

mongodump --username $MONGO_USERNAME --password $MONGO_PASSWORD --authenticationDatabase admin --db $DB_NAME --out $backup_path

# zip it up
tar -zcvf $current_date.tar.gzip $DB_NAME

# upload to s3
aws s3 cp $backup_path/$current_date.tar.gzip s3://$BUCKET --storage-class REDUCED_REDUNDANCY

# clean up
rm -rf $DB_NAME
rm *.tar.gzip

echo "finished backing up"

today=$(date +%Y-%m-%d)
month_start=$(date +%Y-%m-01)
last_month_end=$(date --date="$month_start-1day" +%Y-%m-%d)
last_month_start=$(date --date="$last_month_end" +%Y-%m-01)
last_month_mask=$(date --date="$last_month_start" +%Y-%m)

echo "##########################################################"
echo `date`
echo "pruning $DB_NAME backups from S3 bucket: $BUCKET for "
echo "last month: $last_month_mask..."
echo "##########################################################"

# items=$(aws s3 ls s3://$BUCKET/2020-01)
items=$(aws s3api list-objects --bucket $BUCKET --query 'Contents[*].Key' --prefix $last_month_mask --output text)
for item in $items; do
    if echo "$item" | grep -q "$last_month_end"; then
        echo "keep $item"
    else
        echo "prune $item"
        aws s3 rm s3://$BUCKET/$item
    fi
done

echo "finished pruning, thanks for playing"
