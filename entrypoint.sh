#!/bin/bash

echo "running endpoint.sh with $0"

echo MONGO_USERNAME: $MONGO_USERNAME
echo MONGO_PASSWORD: $MONGO_PASSWORD

if [ -z "$MONGO_USERNAME" ]; then
    echo "MONGO_USERNAME is required"
    exit 1
fi

if [ -z "$MONGO_PASSWORD" ]; then
    echo "MONGO_PASSWORD is required"
    exit 1
fi

dbPath=/data/db
pidfile="${TMPDIR:-/tmp}/docker-entrypoint-temp-mongod.pid"
logfile="${dbPath}/docker-initdb.log"
echo running mongod...
# su-exec mongodb mongod --bind_ip 127.0.0.1 --port 27017 --sslMode disabled --logpath "$logfile" --logappend --pidfilepath "$pidfile" --fork
mongod --bind_ip 127.0.0.1 --port 27017 --sslMode disabled --logpath "$logfile" --logappend --pidfilepath "$pidfile" --fork

mongo --host 127.0.0.1 --port 27017 --quiet admin <<-EOJS
    db.createUser({
        user: "$MONGO_USERNAME",
        pwd: "$MONGO_PASSWORD",
        roles: [ { role: 'root', db: 'admin' } ]
    })
EOJS

echo "created a user, shutdown and retart with --auth"
# su-exec mongodb mongod --shutdown
mongod --shutdown
# su-exec mongodb rm -f "$pidfile"
rm -f "$pidfile"

echo "starting mongod"
exec mongod --auth --bind_ip_all
