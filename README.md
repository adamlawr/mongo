# mongo

alpine based docker container for mongodb database server. includes scripts for backing and restoring with aws s3.
no alpine image is available so i duplicated it.

includes a simplified version of the entrypoint script from the official mongo image.
https://github.com/docker-library/mongo/blob/master/4.4/docker-entrypoint.sh
basic steps in entrypoint:
1. start mongo with no --auth
2. create a user
3. restart mongo with -auth

## access with mongo shell
`$ docker exec -it <container> sh`
`$ mongo --username $MONGO_USERNAME --password $MONGO_PASSWORD --authenticationDatabase admin`

## normal useage
from the deploy script of an app or in an ssh session on the server (probably an ec2 instance)
pull the image and start a container
`docker run -d -p 27017:27017 --name mongo --env-file .env  adamlawrence/mongo`

check if crond is running: `$ rc-service -l | grep crond`

## restore a database
```
$ docker exec -it mongo sh
]$ ./root/restore.sh <s3_file>
```

restore the db from a backup on s3
`$ docker exec mongo /root/restore.sh <s3_file_name>`

restore the db from a backup on s3 if the container is not already running with the environment variables
`$ docker exec --env-file .env  mongo /root/restore.sh <s3_file_name>`

get in and look around:
`docker exec -it mongo sh`

start an interactive mongo shell
`docker exec -it mongo mongo`

## development

### links
- [mongodb manual](https://docs.mongodb.com/manual/)
- [mongo shell manual](https://docs.mongodb.com/manual/mongo/)
- [aws cli manual](https://docs.aws.amazon.com/cli/)
- [aws cli github](https://github.com/aws/aws-cli)
- dockefile partly from [here](https://github.com/DannyBen/docker-alpine-mongo/blob/master/Dockerfile)

### s3
s3 access with `raceweb-s3` or `raceweb-s3-app` iam user

build:
```
docker rm -vf mongo
docker rmi adamlawrence/mongo
docker build -t adamlawrence/mongo .
docker run -d -p 27017:27017 -e AWS_ACCESS_KEY_ID=... -e AWS_SECRET_ACCESS_KEY=... -e DB_NAME=... -e BUCKET=... --name mongo adamlawrence/mongo
```

## deploy gitlab container registry
login
`$ docker login registry.gitlab.com`
build and push image to gitlab
```
docker build -t registry.gitlab.com/adamlawr/mongo:20200214 .
docker push registry.gitlab.com/adamlawr/mongo:20200214
```
