#!/bin/sh

restore_path=/root/restore
file_name=$1

if [ -z "$DB_NAME" ] || [ -z "$BUCKET" ]; then
    echo "BUCKET and DB_NAME need to exist in the env"
    exit 1
fi

echo "##########################################################"
echo `date`
echo "restoring db $DB_NAME from $file_name..."
echo "#########################################################"

if [ -d $restore_path ]; then
    echo "$restore_path exists. cleaning it out..."
    rm -rf $restore_path/*
    rm $restore_path/*.tar.gzip
else
    echo "creating $restore_path"
    mkdir $restore_path
fi
cd $restore_path

# get the backup from s3
aws s3 cp s3://$BUCKET/$file_name $restore_path

# abort if the download didn't work
if [ -e $file_name ]; then
    echo "downloaded $file_name. unzipping it..."
    tar -xzvf $file_name
    # all of the s3 backups are for brainpad_production. rename the exploded
    # directory to match the database name we're trying to restore here
    mv $restore_path/brainpad_production $restore_path/$DB_NAME
else
    echo "$restore_path/$file_name not found. does it exist on s3 in bucket: $BUCKET?"
    exit 1
fi

# import all the things
mongorestore --username $MONGO_USERNAME --password $MONGO_PASSWORD --authenticationDatabase admin -d $DB_NAME --drop $DB_NAME

# clean up
rm -rf $DB_NAME
rm *.tar.gzip

echo 'finished. thanks for coming out'
