FROM alpine:3.9

ENV TERM=linux

# RUN addgroup -S mongodb && adduser -S mongodb -G mongodb && \
# chown -R mongodb:mongodb /data/db && \
RUN apk add --no-cache bash su-exec mongodb mongodb-tools coreutils tzdata && \
    apk -Uuv add groff less python py-pip && \
    pip install awscli && \
    apk --purge -v del py-pip && \
    rm /var/cache/apk/* && \
    mkdir -p /data/db && \
    cp /usr/share/zoneinfo/America/Vancouver /etc/localtime && \
    echo "America/Vancouver" > /etc/timezone

VOLUME /data/db
COPY entrypoint.sh backup.sh restore.sh /usr/local/bin/
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 27017
CMD ["mongod"]
